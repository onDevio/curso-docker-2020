FROM node:13.10.1-alpine3.10
ADD scripts/index.js /scripts/
EXPOSE 3000
CMD node /scripts/index.js
