import {bootstrap} from 'angular2/platform/browser';
import TodoApp from './app'
import {TodoStore} from './services/store';
import { HttpClientModule }    from '@angular/common/http';

bootstrap(TodoApp, [TodoStore]);
