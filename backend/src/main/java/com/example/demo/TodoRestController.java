package com.example.demo;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class TodoRestController {
	
	@Autowired
	private TodoRepository repository;
	
	@PostMapping("/todos")
	public ResponseEntity<Todo> createTodo(@RequestBody TodoDto todoDto) throws URISyntaxException {
		Todo todo = new Todo();
		todo.setTitle(todoDto.get_title());
		repository.save(todo);
		return ResponseEntity.created(new URI("/todos/" + todo.getId())).body(todo);
	}


}
