# 1. Bienvenidos
Docker \
& Docker Compose

Arrancad en **Linux**
Usuario: duser
Contraseña: cursodocker

# 2. Mariano Eloy Fernández Osca - Ondevio
# 3. Qué es Docker
Docker es una **herramienta**
para empaquetar aplicaciones en una **imagen**,
ejecutarlas en un **contenedor**, 
gestionar sus interfaces de **red**
y montar **volúmenes** de ficheros.

# 3. Docker

Docker facilita:
1. Instalar en el mismo equipo diferentes versiones de software (alternatives)
2. Instalar software que tiene dependencias no compatibles con el sistema operativo del host.
3. Desplegar aplicaciones en producción (apt-get, yum)
4. Start/Stop de servicios y aplicaciones (service, systemctl)
5. Empaquetar aplicaciones (.rpm, .deb, .msi)

# 3. Para qué sirve Docker

Docker te permite desarrollar una aplicación en local,
integrarla con otros componentes de producción en local,
y después subirla a producción con garantías de que funcione.

# 4. Cómo funciona Docker
img partes de docker https://www.codementor.io/blog/docker-technology-5x1kilcbow
img docker vs mv https://www.docker.com/resources/what-container
img ezine process https://twitter.com/b0rk/status/1235754793253171200/photo/1

# 5. Contenedores: Comandos básicos
docker run 
docker ps
docker logs
docker start stop 
docker exec
docker attach
docker inspect
docker rm

# Ejercicio: carpeta
mkdir $HOME/curso-docker
cd $HOME/curso-docker
pwd
ls

## 5.1 Ejercicio hello-world
# nombre automático
docker run hello-world
# todos los contenedores
docker ps -a
# solo los que están corriendo
docker ps
# nombre específico
docker run --name hola hello-world
docker ps -a

## 5.1b Ejercicio (alpine)

docker run --rm -it alpine:3.11.3

ls
curl
apk add curl
curl http://www.google.com
exit


## 5.1b Ejercicio (xeyes)

xhost +

docker run --rm -it --net=host --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" alpine:3.11.3
apk add xeyes
xeyes
exit

xhost -


## 5.2 Ejercicio node (-d, logs)
// https://blog.risingstack.com/your-first-node-js-http-server/
docker run --name node -d node:13.10.1-alpine3.10 -e 'require("http").createServer((req, res) => {console.log("Consola!");res.end("Hola!\n")}).listen(3000)'
docker ps
docker inspect node 
docker inspect node | grep IPA
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'
curl -v http://172.17.0.X:3000
docker logs node
docker rm -f node

## 5.3 Ejercicio node (volumen host)
mkdir scripts

cat <<EOF > ./scripts/index.js
const http = require("http")
const server = http.createServer((req, res) => {
    console.log("Consola!");
    res.end("Hola!\n")
    })
server.listen(3000)
EOF

docker run --name node -v $PWD/scripts/index.js:/scripts/index.js -d node:13.10.1-alpine3.10 node /scripts/index.js
docker ps
docker inspect node | grep IPA
curl -v http://172.17.0.X:3000
docker stop node
curl -v http://172.17.0.X:3000
docker start node
curl -v http://172.17.0.X:3000
docker rm -f node

## 5.3 Ejercicio node (port host)
docker run --name node -p 80:3000 -v $PWD/scripts/index.js:/scripts/index.js -d node:13.10.1-alpine3.10 node /scripts/index.js
docker ps
curl -v http://localhost
docker rm -f node

# 6. Imágenes
hub.docker.com

# 6.1 Comandos básicos
docker build
docker image ls
docker image rm

# 6.2 Ejercicio: Dockerfile

cat <<EOF > Dockerfile
FROM node:13.10.1-alpine3.10
ADD scripts/index.js /scripts/
EXPOSE 3000
CMD node /scripts/index.js
EOF

docker build -t node-img .

docker run --name node-con -d node-img

docker ps
docker inspect | grep IPA


# 6.3 Publicar la imagen
docker push node-img

docker login

# 7. Ejercicio: web app backend/frontend

img arquitectura back/front/bdd

http://todomvc.com/

https://github.com/tastejs/todomvc/tree/gh-pages/examples/angular2

## 7.1. Frontend Angular2 (npm run dev)

cd angular2

docker run --rm --name node-dev -it -v $PWD:/app --workdir=/app node:13.10.1-alpine3.10 sh -c 'npm i && npm run dev'

edit store.ts "Hola Mundo!"

https://hub.docker.com/_/nginx

docker run --rm --name nginx -p 80:80 -v $PWD:/usr/share/nginx/html nginx:1.17.9-alpine

http://localhost --> F12 --> Console

## 7.2 Frontend (XHR)

	add(title: String) {
        var tarea = new Todo(title)
		this.todos.push(tarea);
		this.updateStore();
		console.log("Hola Mundo!")
		var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
		var theUrl = "/tareas";
		xmlhttp.open("POST", theUrl);
		xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		xmlhttp.send(JSON.stringify(tarea));
		console.log("XHR!")
	}


## 7.3 Frontend (build)

https://docs.docker.com/develop/develop-images/multistage-build/

cat <<EOF > Dockerfile
FROM node:13.10.1-alpine3.10 as stage1
ADD . /app/
RUN cd app && npm i

FROM nginx:1.17.9-alpine
COPY --from=stage1 /app /usr/share/nginx/html/
EXPOSE 80
EOF

docker build -t front-img .

# 8. PostgreSQL

https://hub.docker.com/_/postgres

docker run --name postgres -p 5432:5432 -e POSTGRES_PASSWORD=postgres -d postgres:12.2-alpine


Abrir DBeaver


# 9. Backend

https://start.spring.io/

web, jpa, postgresql, lombok, dev

cd backend

import maven project

classes @Controller, repository

9.1 Backend (Dockerfile)

https://hub.docker.com/_/maven






# 10 Docker Compose 

https://docs.docker.com/compose/


cat << EOF > docker-compose.yml
version: '3'
services:

  backend:
    image: backend-img
    ports:
    - "8080:8080"

  frontend:
    image: frontend-img
    ports:
    - "3000:80"

  postgres:
    image: postgres:12.2-alpine
    ports:
      - "5432:5432"
    environment:
      - POSTGRES_PASSWORD=postgres
EOF

# 10.1 Docker Compose (traefik)

https://hub.docker.com/_/traefik

https://docs.traefik.io/v2.0/getting-started/quick-start/

version: '3'
services:

  backend:
    image: backend-img
    ports:
    - "8080:8080"
    labels:
      - "traefik.http.routers.backend.rule=PathPrefix(`/todos`)"

  frontend:
    image: frontend-img
    ports:
    - "3000:80"
    labels:
      - "traefik.http.routers.frontend.rule=PathPrefix(`/`)"

  postgres:
    image: postgres:12.2-alpine
    ports:
      - "5432:5432"
    environment:
      - POSTGRES_PASSWORD=postgres
    labels:
      - "traefik.enable=false"

  traefik:
    image: traefik:v2.1.6
    command: --api.insecure=true --providers.docker
    ports:
      - "80:80"
      - "4080:8080"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock